# Logo per language

## Introduction

This module allows admins to set logo for language.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/lpl).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/lpl).

## Requirements

N/A

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Configure the languages logo at theme config page
(admin/appearance/settings/THEME_NAME).

## Maintainers

* [abu zakham (abu-zakham)](https://www.drupal.org/user/2836067)

This project has been sponsored by:
* [Vardot](https://www.drupal.org/vardot) -
  Sponsored initial development, evolutions, maintenance and support.
